<?php
require_once("conexao.php");
require_once("../Modelo/modeloUser.php");
class Login{
        function verificarLogin($nome,$senha){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM users WHERE nome_user = :n AND senha_user = :s;");
                $cmd->bindParam("n", $nome);
                $cmd->bindParam("s", $senha);
                if($cmd->execute()){
                    if($cmd->rowCount() == 1){
                        return true;
                    }else{
                        return false;
                    }
                }
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
    }
?>