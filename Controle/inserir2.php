<?php
    $file = $_FILES['img2'];
    $arq = file_get_contents($file['tmp_name']);
    try{
        require_once('conexao.php');
        $con = new Conexao();
        $sql = "INSERT INTO img2(name,tipo,bin) VALUES(:n,:t,:b);";
        $cmd = $con->getConexao()->prepare($sql);
        $cmd->bindParam('n',$file['name']);
        $cmd->bindParam('t',$file['type']);
        $cmd->bindParam('b',$arq);
        $cmd->execute();
    }catch(Exception $ex){
        echo $ex->getMessage();
    }
  
    header('Location:../Visual/modify.php');
?>
